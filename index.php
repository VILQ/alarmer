<?php
date_default_timezone_set('Europe/Warsaw');

//http://alarmer.tk/?loc=52.228391,21.0263&msg=Wiktor%20Banach%20need%20your%20help.%20He%27s%20near%20!!LOC!!&phones=509988585,607828719&mails=vilq@naprezydenta.com,wtmb@poczta.onet.pl
//http://alarmer.tk/?loc=52.228391,21.0263&msg=Wiktor%20Banach%20need%20your%20help.%20He%27s%20near%20!!LOC!!&phones=509988585,607828719&mails=vilq@naprezydenta.com,wtmb@poczta.onet.pl&debug

function get_location($loc)
{
    $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".$loc."&sensor=false";

    $curl = curl_init();
    $opts = array(
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_TIMEOUT => 60,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_HEADER => 0,
        CURLOPT_SSL_VERIFYPEER => FALSE,
    );
    curl_setopt_array($curl, $opts);
    $result = curl_exec($curl);
    curl_close($curl);
    $output = json_decode($result,1);
    return $output['results'][0]['formatted_address'];
}

function notify_by_twilio($phones, $message)
{
    require 'lib/twilio/Services/Twilio.php';
    $version = "2010-04-01";
    $sid = 'AC916b36d84c99c72c01d4b77b1ef8171b';
    $token = 'c328becf3e4c7d3823713ec1a2a0c751';

    $phonenumber = '48128810857';
    $client = new Services_Twilio($sid, $token, $version);

    foreach ($phones as $phone) {
        if ( substr($phone,0,2) != '48' ){
            $phone = '48'.$phone;
        }
        try {
            $call = $client->account->calls->create(
                $phonenumber, // The number of the phone initiating the call
                $phone, // The number of the phone receiving call
                'http://alarmer.tk/lib/twilio/message.php?message=' . urlencode($message) // The URL Twilio will request when the call is answered
            );
        } catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}

$lat = '52.228391';
$lon = '21.025815';
$loc = '';
$message = '';
$phones = array();
$mails = array();

$data = $_GET;

if (!empty($data['loc'])){
    $loc = get_location($data['loc']);
}
if (!empty($data['msg'])){
    $message = urldecode(str_replace('!!LOC!!',$loc,$data['msg']));
}
if (!empty($data['phones'])){
    $phones = explode(',',$data['phones']);
}
if (!empty($data['mails'])){
    $mails = explode(',',$data['mails']);
}
if (isset($data['debug'])){
    echo '<pre>';
    print_r($data);
    echo json_encode( array('message'=>$message,'loc'=>$loc,'phones'=>$phones,'mails'=>$mails) );
} else {
    if ( !empty($phones) ){
        notify_by_twilio($phones,$message);
    }
    if ( !empty($mails) ){
        $message .= "\n\n";
        $message .= 'GPS Latitude - '.$lat."\n";
        $message .= 'GPS Longitude - '.$lon."\n";
        require_once ('lib/smtpmail/PHPMailer.php');
        $mail = new PHPMailer();
        $mail->IsSMTP();
        $mail->CharSet = 'utf-8';
        $mail->Username = 'alarmer.android@gmail.com';
        $mail->Password = 'wiktor1989';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = "tls";
        $mail->SetFrom('alarmer.android@gmail.com','Alarmer');
        $mail->Subject = 'HELP!';
        $mail->MsgHTML( $message );
        foreach ($mails as $to){
            $mail->AddCC($to);
        }
        $mail->Send();
    }
    echo json_encode(array('OK'=>'Help requested'));
}
?>